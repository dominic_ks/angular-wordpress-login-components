import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { WpLoginService } from '../../../angular-wordpress-rest-api/services/wp-login/wp-login.service';
import { WpUserService } from '../../../angular-wordpress-rest-api/services/wp-user/wp-user.service';

@Component({
  selector: 'angular-wordpress-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class AngularWordpressRegisterComponent implements OnInit {

  form = new FormGroup({});

  model = {
    email: '',
    password: '',
    firstName: '',
    lastName: '',
  };

  fields: FormlyFieldConfig[] = [
    {
      key: 'firstName',
      type: 'text',
      templateOptions: {
        label: 'first name',
        placeholder: 'enter your first name',
        required: true,
        autoFocus: true,
      },
    },
    {
      key: 'lastName',
      type: 'text',
      templateOptions: {
        label: 'last name',
        placeholder: 'enter your last name',
        required: true,
      },
    },
    {
      key: 'email',
      type: 'email',
      templateOptions: {
        label: 'email address',
        placeholder: 'enter email',
        required: true,
      },
      validators: {
        validation: [
          'email',
        ],
      },
    },
    {
      key: 'password',
      type: 'password',
      templateOptions: {
        label: 'password',
        placeholder: 'enter password',
        required: true,
      },
      validators: {
        validation: [
          'password',
        ],
      },
    },
    {
      key: 'submit',
      type: 'primary-submit',
      templateOptions: {
        label: 'Submit',
      },
    },
  ];

  constructor(
    private wpLoginService: WpLoginService,
    private wpUserService: WpUserService,
  ) { }

  ngOnInit() {}

  onSubmit() {

    this.model['loading'] = true;

    this.wpLoginService.registerUser({
      username: this.model['email'],
      email: this.model['email'],
      password: this.model['password'],
    }).subscribe(

      resp => {
        return this.login();
      },

      error => {
        this.model['loading'] = false;
      }

    );

  }

  login() {

    this.wpLoginService.login({
      username: this.model['email'],
      password: this.model['password'],
    }).subscribe(

      resp => {
        return this.updateUserData();
      },

      error => {
        this.model['loading'] = false;
      }

    );

  }

  updateUserData() {

    this.wpUserService.updateUser({
      id: 'me',
      first_name: this.model['firstName'],
      last_name: this.model['lastName'],
      name: this.model['firstName'] + ' ' + this.model['lastName'],
    }).subscribe(

      resp => {
        this.model['loading'] = false;
      },

      error => {
        this.model['loading'] = false;
      }

    );

  }

}
