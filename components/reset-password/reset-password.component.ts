import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { WpLoginService } from '../../../angular-wordpress-rest-api/services/wp-login/wp-login.service';

@Component({
  selector: 'angular-wordpress-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class AngularWordpressResetPasswordComponent implements OnInit {

  public stage: string;

  public requestForm = new FormGroup({});
  public resetForm = new FormGroup({});

  public model = {
    email: '',
  };

  public requestFields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'email',
      templateOptions: {
        label: 'email address',
        placeholder: 'enter email',
        required: true,
        autoFocus: true,
      },
      validators: {
        validation: [
          'email',
        ],
      },
    },
    {
      key: 'submit',
      type: 'primary-submit',
      templateOptions: {
        label: 'Submit',
      },
    },
  ];

  public resetFields: FormlyFieldConfig[] = [
    {
      key: 'code',
      type: 'text',
      templateOptions: {
        label: 'code',
        placeholder: 'enter your reset code',
        required: true,
        autoFocus: true,
        lines: 'none',
      },
      validators: {
        validation: [
          'code',
        ],
      },
    },
    {
      key: 'password',
      type: 'password',
      templateOptions: {
        label: 'password',
        placeholder: 'enter new password',
        required: true,
      },
      validators: {
        validation: [
          'password',
        ],
      },
    },
    {
      key: 'submit',
      type: 'primary-submit',
      templateOptions: {
        label: 'Submit',
      },
    },
  ];

  requestReset() {

    this.model['loading'] = true;

    this.wpLoginService.resetPassword({
      email: this.model['email'],
    }).subscribe(

      resp => {
        this.stage = 'reset-password';
        this.model['loading'] = false;
      },

      error => {
        this.model['loading'] = false;
      }

    );

  }

  resetPassword() {

    this.model['loading'] = true;

    this.wpLoginService.setNewPassword({
      email: this.model['email'],
      code: this.model['code'],
      password: this.model['password'],
    }).subscribe(

      resp => {
        this.stage = 'reset-password';
        this.model['loading'] = false;
      },

      error => {
        this.model['loading'] = false;
      }

    );

  }

  startOver() {
    this.stage = 'request-reset';
  }

  constructor(
    private wpLoginService: WpLoginService,
  ) {
    this.startOver();
  }

  ngOnInit() {}

}
