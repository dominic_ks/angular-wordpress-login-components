import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { WpLoginService } from '../../../angular-wordpress-rest-api/services/wp-login/wp-login.service';

@Component({
  selector: 'angular-wordpress-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class AngularWordpressLoginComponent implements OnInit {

  form = new FormGroup({});

  model: Object = {};

  fields: FormlyFieldConfig[] = [
    {
      key: 'username',
      type: 'email',
      templateOptions: {
        label: 'email address',
        placeholder: 'enter email',
        required: true,
        autoFocus: true,
      },
      validators: {
        validation: [
          'email',
        ],
      },
    },
    {
      key: 'password',
      type: 'password',
      templateOptions: {
        label: 'password',
        placeholder: 'enter password',
        required: true,
      },
    },
    {
      key: 'submit',
      type: 'primary-submit',
      templateOptions: {
        label: 'submit',
      },
    },
  ];

  constructor(
    private wpLoginService: WpLoginService,
  ) { }

  ngOnInit() {}

  onSubmit() {

    this.model['loading'] = true;

    this.wpLoginService.login( this.model ).subscribe(

      resp => {
        this.model['loading'] = false;
      },

      error => {
        this.model['loading'] = false;
      }

    );

  }

}
