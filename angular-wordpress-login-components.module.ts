import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { FormSettingsModule } from '../modules/form-settings/form-settings.module';

import { AngularWordpressLoginComponent } from './components/login/login.component';
import { AngularWordpressRegisterComponent } from './components/register/register.component';
import { AngularWordpressResetPasswordComponent } from './components/reset-password/reset-password.component';

@NgModule({
  declarations: [
    AngularWordpressLoginComponent,
    AngularWordpressRegisterComponent,
    AngularWordpressResetPasswordComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormSettingsModule,
  ],
  exports: [
    AngularWordpressLoginComponent,
    AngularWordpressRegisterComponent,
    AngularWordpressResetPasswordComponent,
  ]
})
export class AngularWordpressLoginComponentsModule { }
